/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author untao
 */
public class TestReceipt {

    public static void main(String[] args) {
        Product p1 = new Product(1, "Chanum", 45);
        Product p2 = new Product(2, "Chanum1", 50);
        User seller = new User("Pawit", "998899", "password");
        Customer customer = new Customer("Payut", "889998");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addRecieptDetail(p1, 1);
        receipt.addRecieptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addRecieptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addRecieptDetail(p1, 2);
        System.out.println(receipt);

    }
}
